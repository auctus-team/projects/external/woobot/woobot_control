#include "FTDriver.hpp"


#define BYTE_MASK 0xFF


///    Static attributes
int FTDriver::port = 49152;
//

/// Default
FTDriver::FTDriver(const std::string& ipAddress) : address(ipAddress)
{
    udpsocket = new UDPsocket(port);
}
FTDriver::~FTDriver()
{
    stop();
    delete udpsocket;
}
FTDriver::Record::Record() : RDTSequence(0), FTSequence(0), status(0), Fx(0), Fy(0), Fz(0), Tx(0), Ty(0), Tz(0){}
FTDriver::Record::Record(const uint8_t* buffer)
{
    unpack(buffer);
}
//

/// Public functions
void FTDriver::start()
{
    uint8_t r[sizeof(Request)];
    Request(Request::START, 0).pack(r);
    udpsocket->sendMessageTo(r, sizeof(Request), address.c_str(), std::to_string(port).c_str());
}
void FTDriver::stop()
{
    uint8_t r[sizeof(Request)];
    Request(Request::STOP, 0).pack(r);
    udpsocket->sendMessageTo(r, sizeof(Request), address.c_str(), std::to_string(port).c_str());
}
FTDriver::Record FTDriver::getLatest()
{
    Record r;
    std::vector<uint8_t> data = udpsocket->read();
    if(!data.empty())
        r = Record(data.data());
    return r;
}
//

/// Set/get functions
void FTDriver::Request::pack(uint8_t* buffer) const
{
    buffer[0] = (header >> 8) & BYTE_MASK;
    buffer[1] = (header >> 0) & BYTE_MASK;
    buffer[2] = (command >> 8) & BYTE_MASK;
    buffer[3] = (command >> 0) & BYTE_MASK;
    buffer[4] = (sampleCount >> 24) & BYTE_MASK;
    buffer[5] = (sampleCount >> 16) & BYTE_MASK;
    buffer[6] = (sampleCount >> 8) & BYTE_MASK;
    buffer[7] = (sampleCount >> 0) & BYTE_MASK;
}
uint32_t FTDriver::Record::unpack32(const uint8_t* buffer)
{
    return ( uint32_t(buffer[0]) << 24) | ( uint32_t(buffer[1]) << 16) | ( uint32_t(buffer[2]) << 8 ) | ( uint32_t(buffer[3]) << 0 ) ;
}
void FTDriver::Record::unpack(const uint8_t *buffer)
{
  RDTSequence = unpack32(buffer + 0);
  FTSequence = unpack32(buffer + 4);
  status = unpack32(buffer + 8);
  Fx = unpack32(buffer + 12);
  Fy = unpack32(buffer + 16);
  Fz = unpack32(buffer + 20);
  Tx = unpack32(buffer + 24);
  Ty = unpack32(buffer + 28);
  Tz = unpack32(buffer + 32);
}
//
