#ifndef FTDRIVER_HPP_INCLUDED
#define FTDRIVER_HPP_INCLUDED


#include "UDP.hpp"


/*! \class FTDriver
 *  \brief Driver class for the ATI force and torque sensor
 *
 *  The class implement helper function to interact with the sensor such as : start streaming data, stop and get the last data
 *
 */
class FTDriver
{
    public:
        /// Micselenaous
        /*! \struct Record
         *  \brief Little struct to represent a data from the sensor
         */
        struct Record
        {
            /*!
             *  \brief Default constructor
             */
            Record();
            /*!
             *  \brief Constructor
             *  \param buffer : construct record from buffer
             */
            Record(const uint8_t* buffer);
            
            
            uint32_t RDTSequence = 0;  //!< The Raw Data Transfer sequence number
            uint32_t FTSequence = 0;   //!< The Force Torque sequence (the ADC sequence)
            uint32_t status = 0;       //!< Sensor status according to thid data
            int32_t Fx,Fy,Fz;          //!< Force values (x,y,z)
            int32_t Tx,Ty,Tz;          //!< Torque values (x,y,z)
            
            /*!
             *  \brief read the buffer and extract value from it
             *  \param buffer : the buffer from wich read the value
             */
            void unpack(const uint8_t *buffer);
            
            /*!
             *  \brief unpack an int32 from buffer (because the sensor works in MSB first)
             *  \param buffer : the buffer from wich read the value
             *  \return the unpacked integer
             */
            uint32_t unpack32(const uint8_t* buffer);
        };
        //
        
        /// Default
        /*!
         *  \brief Constructor
         *  \param ipAddress : the sensor IP adress
         */
        FTDriver(const std::string& ipAddress);
        
        /*!
         *  \brief Destructor
         */
        ~FTDriver();
        //
        
        /// Public functions
        /*!
         *  \brief Start the data streaming
         */
        void start();
        
        /*!
         *  \brief Stop data streaming
         */
        void stop();
        
        /*!
         *  \brief Get the latest data (or an "empty" record if no data was received)
         *  \param ipAddress : the sensor IP adress
         */
        Record getLatest();
        //
        
    protected:
        /// Micselenaous
        /*! \struct Request
         *  \brief Little struct to represent request datagram
         */
        struct Request
        {
            /*!
             *  \brief Default constructor
             *  \param c : request command
             *  \param s : request sample count (0=streaming forever)
             */
            Request(const uint16_t& c, const uint32_t& s) : command(c), sampleCount(s) {};
            
            uint16_t header = 0x1234;   //!< Request header
            uint16_t command;           //!< Command code (see enum)
            uint32_t sampleCount;       //!< Requested sample count (0=forever)
            
            /*! \enum RequestCommands
             *  \brief Enumeration of all request command code available (by the driver)
             */
            enum RequestCommands
            {
                STOP = 0,   //!< Command to stop streaming
                START = 2   //!< Command to start streaming
            };
            
            /*!
             *  \brief pack the request in a sendable datagram (and readable by sensors)
             *  \param buffer : the resulting buffer
             */
            void pack(uint8_t* buffer) const;
        };
        //
        
        /// Attributes
        static int port;         //!< Sensor port (always equals to 49152)
        UDPsocket* udpsocket;    //!< Pointer to an UDP socket
        std::string address;     //!< Sensor IP address (define in constructor)
        //
};

#endif // FTDRIVER_HPP_INCLUDED
