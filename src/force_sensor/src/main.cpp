

//    includes
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "FTDriver.hpp"
#include <geometry_msgs/WrenchStamped.h>

#include <sstream>
#include <iostream>

//    constants
#define MESSAGE_QUEUE 10
#define LOOP_RATE 100
#define SCALE_FACTOR 1000000.0
#define SENSOR_IP_ADDRESS "172.16.0.11"

#include "UDP.hpp"


// node process
int main(int argc, char **argv)
{
    // initialize node and message topics
    ros::init(argc, argv, "lorenzAttractor");
    ros::NodeHandle n;
    ros::Publisher LeftWrench_pub = n.advertise<geometry_msgs::WrenchStamped>("LeftWrench", MESSAGE_QUEUE);
  //  ros::Publisher torque_pub = n.advertise<geometry_msgs::Vector3Stamped>("torque_stream", MESSAGE_QUEUE);
    ros::Rate loop_rate(LOOP_RATE);

    // innitialize messages
    geometry_msgs::WrenchStamped LeftWrench;
  //  force.header.frame_id = "map";
  //  force.header.stamp = ros::Time();
  //  geometry_msgs::Vector3Stamped torque;
  //  torque.header.frame_id = "map";
  //  torque.header.stamp = ros::Time();

    // create socket
    FTDriver sensor(SENSOR_IP_ADDRESS);
    sensor.start();
    double bias_init_fx[10000], bias_init_fy[10000], bias_init_fz[10000];
    double bias_init_tx[10000], bias_init_ty[10000], bias_init_tz[10000];
    double bias_fx{0.0}, bias_fy{0.0}, bias_fz{0.0}, bias_tx{0.0}, bias_ty{0.0}, bias_tz{0.0} ;
    int i{0};

    // infinit loop

    while (ros::ok())
    {
      sensor.start();
      FTDriver::Record r = sensor.getLatest();
      if (i==0)
      {
        i++;
        bias_init_fx[i] = r.Fx/SCALE_FACTOR,bias_init_fy[i] = r.Fy/SCALE_FACTOR,bias_init_fz[i] = r.Fz/SCALE_FACTOR;
        bias_init_tx[i] = r.Tx/SCALE_FACTOR,bias_init_ty[i] = r.Ty/SCALE_FACTOR,bias_init_tz[i] = r.Tz/SCALE_FACTOR;
      }
      if ((i>0)&(i<10001))
      {
      i++;
      bias_init_fx[i] = r.Fx/SCALE_FACTOR,bias_init_fy[i] = r.Fy/SCALE_FACTOR,bias_init_fz[i] = r.Fz/SCALE_FACTOR;
      bias_init_tx[i] = r.Tx/SCALE_FACTOR,bias_init_ty[i] = r.Ty/SCALE_FACTOR,bias_init_tz[i] = r.Tz/SCALE_FACTOR;
      bias_fx = (bias_init_fx[i-1] + bias_init_fx[i])/2;
      bias_fy = (bias_init_fy[i-1] + bias_init_fy[i])/2;
      bias_fz = (bias_init_fz[i-1] + bias_init_fz[i])/2;
      bias_tx = (bias_init_tx[i-1] + bias_init_tx[i])/2;
      bias_ty = (bias_init_ty[i-1] + bias_init_ty[i])/2;
      bias_tz = (bias_init_tz[i-1] + bias_init_tz[i])/2;
      }
    std::cout << "Essai: " << i << std::endl;
    //  else
      //{

        LeftWrench.wrench.force.x = r.Fx/SCALE_FACTOR -  bias_fx;
        LeftWrench.wrench.force.y = r.Fy/SCALE_FACTOR -  bias_fy;
        LeftWrench.wrench.force.z = r.Fz/SCALE_FACTOR -  bias_fz;
        LeftWrench.wrench.torque.x = r.Tx/SCALE_FACTOR - bias_tx;
        LeftWrench.wrench.torque.y = r.Ty/SCALE_FACTOR - bias_ty;
        LeftWrench.wrench.torque.z = r.Tz/SCALE_FACTOR - bias_tz;
        LeftWrench.header.stamp = ros::Time::now();
        LeftWrench.header.frame_id = "ati_left_link";

        // send messages
        LeftWrench_pub.publish(LeftWrench);
      //  }
  //      std::cout << force.vector.x << ' ' << force.vector.y << ' ' << force.vector.z << ' ' << torque.vector.x << ' ' << torque.vector.y << ' ' << torque.vector.z << std::endl;

        // end
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
