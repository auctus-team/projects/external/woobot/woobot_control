#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nbenhabi/panda_ws/devel/.private/force_sensor:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/nbenhabi/panda_ws/devel/.private/force_sensor/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/nbenhabi/panda_ws/devel/.private/force_sensor/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/nbenhabi/panda_ws/build/force_sensor'
export ROSLISP_PACKAGE_DIRECTORIES="/home/nbenhabi/panda_ws/devel/.private/force_sensor/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/nbenhabi/panda_ws/src/force_sensor:$ROS_PACKAGE_PATH"