#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nbenhabi/panda_ws/devel/.private/static_laser_tools:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/nbenhabi/panda_ws/devel/.private/static_laser_tools/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/nbenhabi/panda_ws/devel/.private/static_laser_tools/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/nbenhabi/panda_ws/build/static_laser_tools'
export ROSLISP_PACKAGE_DIRECTORIES="/home/nbenhabi/panda_ws/devel/.private/static_laser_tools/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/nbenhabi/panda_ws/src/static_laser_tools:$ROS_PACKAGE_PATH"