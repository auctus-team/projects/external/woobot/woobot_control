#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nbenhabi/panda_ws/devel/.private/controller_manager_msgs:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/nbenhabi/panda_ws/devel/.private/controller_manager_msgs/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/nbenhabi/panda_ws/devel/.private/controller_manager_msgs/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/nbenhabi/panda_ws/build/controller_manager_msgs'
export PYTHONPATH="/home/nbenhabi/panda_ws/devel/.private/controller_manager_msgs/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/nbenhabi/panda_ws/devel/.private/controller_manager_msgs/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/nbenhabi/panda_ws/src/ros_control/controller_manager_msgs:$ROS_PACKAGE_PATH"