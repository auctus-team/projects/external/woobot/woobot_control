# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "rosbag_storage;rosconsole;roscpp;topic_tools;xmlrpcpp;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lrosbag".split(';') if "-lrosbag" != "" else []
PROJECT_NAME = "bag_recorder"
PROJECT_SPACE_DIR = "/home/nbenhabi/panda_ws/install"
PROJECT_VERSION = "0.1.0"
