#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nbenhabi/panda_ws/devel/.private/rosbag_to_csv:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/nbenhabi/panda_ws/devel/.private/rosbag_to_csv/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/nbenhabi/panda_ws/devel/.private/rosbag_to_csv/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/nbenhabi/panda_ws/build/rosbag_to_csv'
export ROSLISP_PACKAGE_DIRECTORIES="/home/nbenhabi/panda_ws/devel/.private/rosbag_to_csv/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/nbenhabi/panda_ws/src/rosbag_to_csv:$ROS_PACKAGE_PATH"