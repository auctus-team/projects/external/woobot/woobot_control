# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "kombos_server: 5 messages, 0 services")

set(MSG_I_FLAGS "-Ikombos_server:/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg;-Ivisualization_msgs:/opt/ros/melodic/share/visualization_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/melodic/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/melodic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(kombos_server_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_custom_target(_kombos_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kombos_server" "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" "kombos_server/positionMsg:geometry_msgs/Point"
)

get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_custom_target(_kombos_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kombos_server" "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" "geometry_msgs/Point"
)

get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_custom_target(_kombos_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kombos_server" "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" "kombos_server/measureKinect:kombos_server/kalmanState:kombos_server/positionMsg:kombos_server/matrixMsg:geometry_msgs/Point"
)

get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_custom_target(_kombos_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kombos_server" "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" ""
)

get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_custom_target(_kombos_server_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kombos_server" "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
)
_generate_msg_cpp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
)
_generate_msg_cpp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
)
_generate_msg_cpp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
)
_generate_msg_cpp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
)

### Generating Services

### Generating Module File
_generate_module_cpp(kombos_server
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(kombos_server_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(kombos_server_generate_messages kombos_server_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_cpp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_cpp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_cpp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_cpp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_cpp _kombos_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kombos_server_gencpp)
add_dependencies(kombos_server_gencpp kombos_server_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kombos_server_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
)
_generate_msg_eus(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
)
_generate_msg_eus(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
)
_generate_msg_eus(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
)
_generate_msg_eus(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
)

### Generating Services

### Generating Module File
_generate_module_eus(kombos_server
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(kombos_server_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(kombos_server_generate_messages kombos_server_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_eus _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_eus _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_eus _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_eus _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_eus _kombos_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kombos_server_geneus)
add_dependencies(kombos_server_geneus kombos_server_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kombos_server_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
)
_generate_msg_lisp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
)
_generate_msg_lisp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
)
_generate_msg_lisp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
)
_generate_msg_lisp(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
)

### Generating Services

### Generating Module File
_generate_module_lisp(kombos_server
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(kombos_server_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(kombos_server_generate_messages kombos_server_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_lisp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_lisp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_lisp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_lisp _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_lisp _kombos_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kombos_server_genlisp)
add_dependencies(kombos_server_genlisp kombos_server_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kombos_server_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
)
_generate_msg_nodejs(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
)
_generate_msg_nodejs(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
)
_generate_msg_nodejs(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
)
_generate_msg_nodejs(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
)

### Generating Services

### Generating Module File
_generate_module_nodejs(kombos_server
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(kombos_server_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(kombos_server_generate_messages kombos_server_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_nodejs _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_nodejs _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_nodejs _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_nodejs _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_nodejs _kombos_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kombos_server_gennodejs)
add_dependencies(kombos_server_gennodejs kombos_server_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kombos_server_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
)
_generate_msg_py(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
)
_generate_msg_py(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg"
  "${MSG_I_FLAGS}"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg;/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
)
_generate_msg_py(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
)
_generate_msg_py(kombos_server
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
)

### Generating Services

### Generating Module File
_generate_module_py(kombos_server
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(kombos_server_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(kombos_server_generate_messages kombos_server_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKinect.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_py _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/positionMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_py _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/measureKalman.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_py _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/kalmanState.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_py _kombos_server_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/kombos-server/msg/matrixMsg.msg" NAME_WE)
add_dependencies(kombos_server_generate_messages_py _kombos_server_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kombos_server_genpy)
add_dependencies(kombos_server_genpy kombos_server_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kombos_server_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kombos_server
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(kombos_server_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET visualization_msgs_generate_messages_cpp)
  add_dependencies(kombos_server_generate_messages_cpp visualization_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(kombos_server_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(kombos_server_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kombos_server
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(kombos_server_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET visualization_msgs_generate_messages_eus)
  add_dependencies(kombos_server_generate_messages_eus visualization_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(kombos_server_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(kombos_server_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kombos_server
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(kombos_server_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET visualization_msgs_generate_messages_lisp)
  add_dependencies(kombos_server_generate_messages_lisp visualization_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(kombos_server_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(kombos_server_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kombos_server
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(kombos_server_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET visualization_msgs_generate_messages_nodejs)
  add_dependencies(kombos_server_generate_messages_nodejs visualization_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(kombos_server_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(kombos_server_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kombos_server
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(kombos_server_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET visualization_msgs_generate_messages_py)
  add_dependencies(kombos_server_generate_messages_py visualization_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(kombos_server_generate_messages_py sensor_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(kombos_server_generate_messages_py geometry_msgs_generate_messages_py)
endif()
