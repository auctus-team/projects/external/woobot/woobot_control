# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/src/NoisySkeleton.cpp" "/home/nbenhabi/panda_ws/build/skeleton_streamer/CMakeFiles/NoisySkeleton_node.dir/src/NoisySkeleton.cpp.o"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/src/UDPConnection.cpp" "/home/nbenhabi/panda_ws/build/skeleton_streamer/CMakeFiles/NoisySkeleton_node.dir/src/UDPConnection.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"skeleton_streamer\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/nbenhabi/panda_ws/devel/.private/skeleton_streamer/include"
  "/usr/include/eigen3"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
