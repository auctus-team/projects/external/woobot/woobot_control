# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "skeleton_streamer: 0 messages, 1 services")

set(MSG_I_FLAGS "-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(skeleton_streamer_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_custom_target(_skeleton_streamer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "skeleton_streamer" "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(skeleton_streamer
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/skeleton_streamer
)

### Generating Module File
_generate_module_cpp(skeleton_streamer
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/skeleton_streamer
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(skeleton_streamer_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(skeleton_streamer_generate_messages skeleton_streamer_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_dependencies(skeleton_streamer_generate_messages_cpp _skeleton_streamer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(skeleton_streamer_gencpp)
add_dependencies(skeleton_streamer_gencpp skeleton_streamer_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS skeleton_streamer_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(skeleton_streamer
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/skeleton_streamer
)

### Generating Module File
_generate_module_eus(skeleton_streamer
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/skeleton_streamer
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(skeleton_streamer_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(skeleton_streamer_generate_messages skeleton_streamer_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_dependencies(skeleton_streamer_generate_messages_eus _skeleton_streamer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(skeleton_streamer_geneus)
add_dependencies(skeleton_streamer_geneus skeleton_streamer_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS skeleton_streamer_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(skeleton_streamer
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/skeleton_streamer
)

### Generating Module File
_generate_module_lisp(skeleton_streamer
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/skeleton_streamer
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(skeleton_streamer_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(skeleton_streamer_generate_messages skeleton_streamer_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_dependencies(skeleton_streamer_generate_messages_lisp _skeleton_streamer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(skeleton_streamer_genlisp)
add_dependencies(skeleton_streamer_genlisp skeleton_streamer_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS skeleton_streamer_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services
_generate_srv_nodejs(skeleton_streamer
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/skeleton_streamer
)

### Generating Module File
_generate_module_nodejs(skeleton_streamer
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/skeleton_streamer
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(skeleton_streamer_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(skeleton_streamer_generate_messages skeleton_streamer_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_dependencies(skeleton_streamer_generate_messages_nodejs _skeleton_streamer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(skeleton_streamer_gennodejs)
add_dependencies(skeleton_streamer_gennodejs skeleton_streamer_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS skeleton_streamer_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(skeleton_streamer
  "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/skeleton_streamer
)

### Generating Module File
_generate_module_py(skeleton_streamer
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/skeleton_streamer
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(skeleton_streamer_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(skeleton_streamer_generate_messages skeleton_streamer_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/nbenhabi/panda_ws/src/kombos-server-v2/skeleton-streamer/srv/SetNoisySkeletonDispersion.srv" NAME_WE)
add_dependencies(skeleton_streamer_generate_messages_py _skeleton_streamer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(skeleton_streamer_genpy)
add_dependencies(skeleton_streamer_genpy skeleton_streamer_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS skeleton_streamer_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/skeleton_streamer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/skeleton_streamer
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(skeleton_streamer_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/skeleton_streamer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/skeleton_streamer
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(skeleton_streamer_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/skeleton_streamer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/skeleton_streamer
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(skeleton_streamer_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/skeleton_streamer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/skeleton_streamer
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(skeleton_streamer_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/skeleton_streamer)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/skeleton_streamer\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/skeleton_streamer
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(skeleton_streamer_generate_messages_py std_msgs_generate_messages_py)
endif()
