#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nbenhabi/panda_ws/devel/.private/qp_solver:$CMAKE_PREFIX_PATH"
export PWD='/home/nbenhabi/panda_ws/build/qp_solver'
export ROSLISP_PACKAGE_DIRECTORIES="/home/nbenhabi/panda_ws/devel/.private/qp_solver/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/nbenhabi/panda_ws/src/qp_solver:$ROS_PACKAGE_PATH"