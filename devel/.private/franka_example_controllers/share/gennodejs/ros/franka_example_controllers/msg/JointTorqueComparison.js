// Auto-generated. Do not edit!

// (in-package franka_example_controllers.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class JointTorqueComparison {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.tau_error = null;
      this.tau_commanded = null;
      this.tau_measured = null;
      this.F_ext_measured = null;
      this.F_ext_desired = null;
      this.F_ext_error = null;
      this.root_mean_square_error = null;
      this.time = null;
      this.V_measured = null;
      this.P_measured = null;
    }
    else {
      if (initObj.hasOwnProperty('tau_error')) {
        this.tau_error = initObj.tau_error
      }
      else {
        this.tau_error = new Array(7).fill(0);
      }
      if (initObj.hasOwnProperty('tau_commanded')) {
        this.tau_commanded = initObj.tau_commanded
      }
      else {
        this.tau_commanded = new Array(7).fill(0);
      }
      if (initObj.hasOwnProperty('tau_measured')) {
        this.tau_measured = initObj.tau_measured
      }
      else {
        this.tau_measured = new Array(7).fill(0);
      }
      if (initObj.hasOwnProperty('F_ext_measured')) {
        this.F_ext_measured = initObj.F_ext_measured
      }
      else {
        this.F_ext_measured = new Array(6).fill(0);
      }
      if (initObj.hasOwnProperty('F_ext_desired')) {
        this.F_ext_desired = initObj.F_ext_desired
      }
      else {
        this.F_ext_desired = new Array(6).fill(0);
      }
      if (initObj.hasOwnProperty('F_ext_error')) {
        this.F_ext_error = initObj.F_ext_error
      }
      else {
        this.F_ext_error = new Array(6).fill(0);
      }
      if (initObj.hasOwnProperty('root_mean_square_error')) {
        this.root_mean_square_error = initObj.root_mean_square_error
      }
      else {
        this.root_mean_square_error = 0.0;
      }
      if (initObj.hasOwnProperty('time')) {
        this.time = initObj.time
      }
      else {
        this.time = 0.0;
      }
      if (initObj.hasOwnProperty('V_measured')) {
        this.V_measured = initObj.V_measured
      }
      else {
        this.V_measured = new Array(6).fill(0);
      }
      if (initObj.hasOwnProperty('P_measured')) {
        this.P_measured = initObj.P_measured
      }
      else {
        this.P_measured = new Array(6).fill(0);
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type JointTorqueComparison
    // Check that the constant length array field [tau_error] has the right length
    if (obj.tau_error.length !== 7) {
      throw new Error('Unable to serialize array field tau_error - length must be 7')
    }
    // Serialize message field [tau_error]
    bufferOffset = _arraySerializer.float64(obj.tau_error, buffer, bufferOffset, 7);
    // Check that the constant length array field [tau_commanded] has the right length
    if (obj.tau_commanded.length !== 7) {
      throw new Error('Unable to serialize array field tau_commanded - length must be 7')
    }
    // Serialize message field [tau_commanded]
    bufferOffset = _arraySerializer.float64(obj.tau_commanded, buffer, bufferOffset, 7);
    // Check that the constant length array field [tau_measured] has the right length
    if (obj.tau_measured.length !== 7) {
      throw new Error('Unable to serialize array field tau_measured - length must be 7')
    }
    // Serialize message field [tau_measured]
    bufferOffset = _arraySerializer.float64(obj.tau_measured, buffer, bufferOffset, 7);
    // Check that the constant length array field [F_ext_measured] has the right length
    if (obj.F_ext_measured.length !== 6) {
      throw new Error('Unable to serialize array field F_ext_measured - length must be 6')
    }
    // Serialize message field [F_ext_measured]
    bufferOffset = _arraySerializer.float64(obj.F_ext_measured, buffer, bufferOffset, 6);
    // Check that the constant length array field [F_ext_desired] has the right length
    if (obj.F_ext_desired.length !== 6) {
      throw new Error('Unable to serialize array field F_ext_desired - length must be 6')
    }
    // Serialize message field [F_ext_desired]
    bufferOffset = _arraySerializer.float64(obj.F_ext_desired, buffer, bufferOffset, 6);
    // Check that the constant length array field [F_ext_error] has the right length
    if (obj.F_ext_error.length !== 6) {
      throw new Error('Unable to serialize array field F_ext_error - length must be 6')
    }
    // Serialize message field [F_ext_error]
    bufferOffset = _arraySerializer.float64(obj.F_ext_error, buffer, bufferOffset, 6);
    // Serialize message field [root_mean_square_error]
    bufferOffset = _serializer.float64(obj.root_mean_square_error, buffer, bufferOffset);
    // Serialize message field [time]
    bufferOffset = _serializer.float64(obj.time, buffer, bufferOffset);
    // Check that the constant length array field [V_measured] has the right length
    if (obj.V_measured.length !== 6) {
      throw new Error('Unable to serialize array field V_measured - length must be 6')
    }
    // Serialize message field [V_measured]
    bufferOffset = _arraySerializer.float64(obj.V_measured, buffer, bufferOffset, 6);
    // Check that the constant length array field [P_measured] has the right length
    if (obj.P_measured.length !== 6) {
      throw new Error('Unable to serialize array field P_measured - length must be 6')
    }
    // Serialize message field [P_measured]
    bufferOffset = _arraySerializer.float64(obj.P_measured, buffer, bufferOffset, 6);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type JointTorqueComparison
    let len;
    let data = new JointTorqueComparison(null);
    // Deserialize message field [tau_error]
    data.tau_error = _arrayDeserializer.float64(buffer, bufferOffset, 7)
    // Deserialize message field [tau_commanded]
    data.tau_commanded = _arrayDeserializer.float64(buffer, bufferOffset, 7)
    // Deserialize message field [tau_measured]
    data.tau_measured = _arrayDeserializer.float64(buffer, bufferOffset, 7)
    // Deserialize message field [F_ext_measured]
    data.F_ext_measured = _arrayDeserializer.float64(buffer, bufferOffset, 6)
    // Deserialize message field [F_ext_desired]
    data.F_ext_desired = _arrayDeserializer.float64(buffer, bufferOffset, 6)
    // Deserialize message field [F_ext_error]
    data.F_ext_error = _arrayDeserializer.float64(buffer, bufferOffset, 6)
    // Deserialize message field [root_mean_square_error]
    data.root_mean_square_error = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [time]
    data.time = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [V_measured]
    data.V_measured = _arrayDeserializer.float64(buffer, bufferOffset, 6)
    // Deserialize message field [P_measured]
    data.P_measured = _arrayDeserializer.float64(buffer, bufferOffset, 6)
    return data;
  }

  static getMessageSize(object) {
    return 424;
  }

  static datatype() {
    // Returns string type for a message object
    return 'franka_example_controllers/JointTorqueComparison';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '29dd5a2956690163cef52317ddda210d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64[7] tau_error
    float64[7] tau_commanded
    float64[7] tau_measured
    float64[6] F_ext_measured
    float64[6] F_ext_desired
    float64[6] F_ext_error
      float64 root_mean_square_error
      float64 time
    float64[6] V_measured
    float64[6] P_measured
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new JointTorqueComparison(null);
    if (msg.tau_error !== undefined) {
      resolved.tau_error = msg.tau_error;
    }
    else {
      resolved.tau_error = new Array(7).fill(0)
    }

    if (msg.tau_commanded !== undefined) {
      resolved.tau_commanded = msg.tau_commanded;
    }
    else {
      resolved.tau_commanded = new Array(7).fill(0)
    }

    if (msg.tau_measured !== undefined) {
      resolved.tau_measured = msg.tau_measured;
    }
    else {
      resolved.tau_measured = new Array(7).fill(0)
    }

    if (msg.F_ext_measured !== undefined) {
      resolved.F_ext_measured = msg.F_ext_measured;
    }
    else {
      resolved.F_ext_measured = new Array(6).fill(0)
    }

    if (msg.F_ext_desired !== undefined) {
      resolved.F_ext_desired = msg.F_ext_desired;
    }
    else {
      resolved.F_ext_desired = new Array(6).fill(0)
    }

    if (msg.F_ext_error !== undefined) {
      resolved.F_ext_error = msg.F_ext_error;
    }
    else {
      resolved.F_ext_error = new Array(6).fill(0)
    }

    if (msg.root_mean_square_error !== undefined) {
      resolved.root_mean_square_error = msg.root_mean_square_error;
    }
    else {
      resolved.root_mean_square_error = 0.0
    }

    if (msg.time !== undefined) {
      resolved.time = msg.time;
    }
    else {
      resolved.time = 0.0
    }

    if (msg.V_measured !== undefined) {
      resolved.V_measured = msg.V_measured;
    }
    else {
      resolved.V_measured = new Array(6).fill(0)
    }

    if (msg.P_measured !== undefined) {
      resolved.P_measured = msg.P_measured;
    }
    else {
      resolved.P_measured = new Array(6).fill(0)
    }

    return resolved;
    }
};

module.exports = JointTorqueComparison;
