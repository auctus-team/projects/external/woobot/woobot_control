;; Auto-generated. Do not edit!


(when (boundp 'franka_example_controllers::JointTorqueComparison)
  (if (not (find-package "FRANKA_EXAMPLE_CONTROLLERS"))
    (make-package "FRANKA_EXAMPLE_CONTROLLERS"))
  (shadow 'JointTorqueComparison (find-package "FRANKA_EXAMPLE_CONTROLLERS")))
(unless (find-package "FRANKA_EXAMPLE_CONTROLLERS::JOINTTORQUECOMPARISON")
  (make-package "FRANKA_EXAMPLE_CONTROLLERS::JOINTTORQUECOMPARISON"))

(in-package "ROS")
;;//! \htmlinclude JointTorqueComparison.msg.html


(defclass franka_example_controllers::JointTorqueComparison
  :super ros::object
  :slots (_tau_error _tau_commanded _tau_measured _F_ext_measured _F_ext_desired _F_ext_error _root_mean_square_error _time _V_measured _P_measured ))

(defmethod franka_example_controllers::JointTorqueComparison
  (:init
   (&key
    ((:tau_error __tau_error) (make-array 7 :initial-element 0.0 :element-type :float))
    ((:tau_commanded __tau_commanded) (make-array 7 :initial-element 0.0 :element-type :float))
    ((:tau_measured __tau_measured) (make-array 7 :initial-element 0.0 :element-type :float))
    ((:F_ext_measured __F_ext_measured) (make-array 6 :initial-element 0.0 :element-type :float))
    ((:F_ext_desired __F_ext_desired) (make-array 6 :initial-element 0.0 :element-type :float))
    ((:F_ext_error __F_ext_error) (make-array 6 :initial-element 0.0 :element-type :float))
    ((:root_mean_square_error __root_mean_square_error) 0.0)
    ((:time __time) 0.0)
    ((:V_measured __V_measured) (make-array 6 :initial-element 0.0 :element-type :float))
    ((:P_measured __P_measured) (make-array 6 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _tau_error __tau_error)
   (setq _tau_commanded __tau_commanded)
   (setq _tau_measured __tau_measured)
   (setq _F_ext_measured __F_ext_measured)
   (setq _F_ext_desired __F_ext_desired)
   (setq _F_ext_error __F_ext_error)
   (setq _root_mean_square_error (float __root_mean_square_error))
   (setq _time (float __time))
   (setq _V_measured __V_measured)
   (setq _P_measured __P_measured)
   self)
  (:tau_error
   (&optional __tau_error)
   (if __tau_error (setq _tau_error __tau_error)) _tau_error)
  (:tau_commanded
   (&optional __tau_commanded)
   (if __tau_commanded (setq _tau_commanded __tau_commanded)) _tau_commanded)
  (:tau_measured
   (&optional __tau_measured)
   (if __tau_measured (setq _tau_measured __tau_measured)) _tau_measured)
  (:F_ext_measured
   (&optional __F_ext_measured)
   (if __F_ext_measured (setq _F_ext_measured __F_ext_measured)) _F_ext_measured)
  (:F_ext_desired
   (&optional __F_ext_desired)
   (if __F_ext_desired (setq _F_ext_desired __F_ext_desired)) _F_ext_desired)
  (:F_ext_error
   (&optional __F_ext_error)
   (if __F_ext_error (setq _F_ext_error __F_ext_error)) _F_ext_error)
  (:root_mean_square_error
   (&optional __root_mean_square_error)
   (if __root_mean_square_error (setq _root_mean_square_error __root_mean_square_error)) _root_mean_square_error)
  (:time
   (&optional __time)
   (if __time (setq _time __time)) _time)
  (:V_measured
   (&optional __V_measured)
   (if __V_measured (setq _V_measured __V_measured)) _V_measured)
  (:P_measured
   (&optional __P_measured)
   (if __P_measured (setq _P_measured __P_measured)) _P_measured)
  (:serialization-length
   ()
   (+
    ;; float64[7] _tau_error
    (* 8    7)
    ;; float64[7] _tau_commanded
    (* 8    7)
    ;; float64[7] _tau_measured
    (* 8    7)
    ;; float64[6] _F_ext_measured
    (* 8    6)
    ;; float64[6] _F_ext_desired
    (* 8    6)
    ;; float64[6] _F_ext_error
    (* 8    6)
    ;; float64 _root_mean_square_error
    8
    ;; float64 _time
    8
    ;; float64[6] _V_measured
    (* 8    6)
    ;; float64[6] _P_measured
    (* 8    6)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64[7] _tau_error
     (dotimes (i 7)
       (sys::poke (elt _tau_error i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[7] _tau_commanded
     (dotimes (i 7)
       (sys::poke (elt _tau_commanded i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[7] _tau_measured
     (dotimes (i 7)
       (sys::poke (elt _tau_measured i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[6] _F_ext_measured
     (dotimes (i 6)
       (sys::poke (elt _F_ext_measured i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[6] _F_ext_desired
     (dotimes (i 6)
       (sys::poke (elt _F_ext_desired i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[6] _F_ext_error
     (dotimes (i 6)
       (sys::poke (elt _F_ext_error i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64 _root_mean_square_error
       (sys::poke _root_mean_square_error (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _time
       (sys::poke _time (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64[6] _V_measured
     (dotimes (i 6)
       (sys::poke (elt _V_measured i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[6] _P_measured
     (dotimes (i 6)
       (sys::poke (elt _P_measured i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64[7] _tau_error
   (dotimes (i (length _tau_error))
     (setf (elt _tau_error i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[7] _tau_commanded
   (dotimes (i (length _tau_commanded))
     (setf (elt _tau_commanded i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[7] _tau_measured
   (dotimes (i (length _tau_measured))
     (setf (elt _tau_measured i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[6] _F_ext_measured
   (dotimes (i (length _F_ext_measured))
     (setf (elt _F_ext_measured i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[6] _F_ext_desired
   (dotimes (i (length _F_ext_desired))
     (setf (elt _F_ext_desired i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[6] _F_ext_error
   (dotimes (i (length _F_ext_error))
     (setf (elt _F_ext_error i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64 _root_mean_square_error
     (setq _root_mean_square_error (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _time
     (setq _time (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64[6] _V_measured
   (dotimes (i (length _V_measured))
     (setf (elt _V_measured i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;; float64[6] _P_measured
   (dotimes (i (length _P_measured))
     (setf (elt _P_measured i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     )
   ;;
   self)
  )

(setf (get franka_example_controllers::JointTorqueComparison :md5sum-) "29dd5a2956690163cef52317ddda210d")
(setf (get franka_example_controllers::JointTorqueComparison :datatype-) "franka_example_controllers/JointTorqueComparison")
(setf (get franka_example_controllers::JointTorqueComparison :definition-)
      "float64[7] tau_error
float64[7] tau_commanded
float64[7] tau_measured
float64[6] F_ext_measured
float64[6] F_ext_desired
float64[6] F_ext_error
  float64 root_mean_square_error
  float64 time
float64[6] V_measured
float64[6] P_measured

")



(provide :franka_example_controllers/JointTorqueComparison "29dd5a2956690163cef52317ddda210d")


