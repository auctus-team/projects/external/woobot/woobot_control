;; Auto-generated. Do not edit!


(when (boundp 'bag_recorder::Rosbag)
  (if (not (find-package "BAG_RECORDER"))
    (make-package "BAG_RECORDER"))
  (shadow 'Rosbag (find-package "BAG_RECORDER")))
(unless (find-package "BAG_RECORDER::ROSBAG")
  (make-package "BAG_RECORDER::ROSBAG"))

(in-package "ROS")
;;//! \htmlinclude Rosbag.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass bag_recorder::Rosbag
  :super ros::object
  :slots (_header _config _bag_name ))

(defmethod bag_recorder::Rosbag
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:config __config) "")
    ((:bag_name __bag_name) "")
    )
   (send-super :init)
   (setq _header __header)
   (setq _config (string __config))
   (setq _bag_name (string __bag_name))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:config
   (&optional __config)
   (if __config (setq _config __config)) _config)
  (:bag_name
   (&optional __bag_name)
   (if __bag_name (setq _bag_name __bag_name)) _bag_name)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; string _config
    4 (length _config)
    ;; string _bag_name
    4 (length _bag_name)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; string _config
       (write-long (length _config) s) (princ _config s)
     ;; string _bag_name
       (write-long (length _bag_name) s) (princ _bag_name s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; string _config
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _config (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _bag_name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _bag_name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get bag_recorder::Rosbag :md5sum-) "6798439af7e30e1ec3307cb93f96c57b")
(setf (get bag_recorder::Rosbag :datatype-) "bag_recorder/Rosbag")
(setf (get bag_recorder::Rosbag :definition-)
      "Header header

string config
string bag_name

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :bag_recorder/Rosbag "6798439af7e30e1ec3307cb93f96c57b")


