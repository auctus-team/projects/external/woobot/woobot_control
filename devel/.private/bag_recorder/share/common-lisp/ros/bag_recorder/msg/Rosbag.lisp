; Auto-generated. Do not edit!


(cl:in-package bag_recorder-msg)


;//! \htmlinclude Rosbag.msg.html

(cl:defclass <Rosbag> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (config
    :reader config
    :initarg :config
    :type cl:string
    :initform "")
   (bag_name
    :reader bag_name
    :initarg :bag_name
    :type cl:string
    :initform ""))
)

(cl:defclass Rosbag (<Rosbag>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Rosbag>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Rosbag)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name bag_recorder-msg:<Rosbag> is deprecated: use bag_recorder-msg:Rosbag instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Rosbag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader bag_recorder-msg:header-val is deprecated.  Use bag_recorder-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'config-val :lambda-list '(m))
(cl:defmethod config-val ((m <Rosbag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader bag_recorder-msg:config-val is deprecated.  Use bag_recorder-msg:config instead.")
  (config m))

(cl:ensure-generic-function 'bag_name-val :lambda-list '(m))
(cl:defmethod bag_name-val ((m <Rosbag>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader bag_recorder-msg:bag_name-val is deprecated.  Use bag_recorder-msg:bag_name instead.")
  (bag_name m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Rosbag>) ostream)
  "Serializes a message object of type '<Rosbag>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'config))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'config))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'bag_name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'bag_name))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Rosbag>) istream)
  "Deserializes a message object of type '<Rosbag>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'config) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'config) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'bag_name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'bag_name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Rosbag>)))
  "Returns string type for a message object of type '<Rosbag>"
  "bag_recorder/Rosbag")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Rosbag)))
  "Returns string type for a message object of type 'Rosbag"
  "bag_recorder/Rosbag")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Rosbag>)))
  "Returns md5sum for a message object of type '<Rosbag>"
  "6798439af7e30e1ec3307cb93f96c57b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Rosbag)))
  "Returns md5sum for a message object of type 'Rosbag"
  "6798439af7e30e1ec3307cb93f96c57b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Rosbag>)))
  "Returns full string definition for message of type '<Rosbag>"
  (cl:format cl:nil "Header header~%~%string config~%string bag_name~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Rosbag)))
  "Returns full string definition for message of type 'Rosbag"
  (cl:format cl:nil "Header header~%~%string config~%string bag_name~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Rosbag>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:length (cl:slot-value msg 'config))
     4 (cl:length (cl:slot-value msg 'bag_name))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Rosbag>))
  "Converts a ROS message object to a list"
  (cl:list 'Rosbag
    (cl:cons ':header (header msg))
    (cl:cons ':config (config msg))
    (cl:cons ':bag_name (bag_name msg))
))
