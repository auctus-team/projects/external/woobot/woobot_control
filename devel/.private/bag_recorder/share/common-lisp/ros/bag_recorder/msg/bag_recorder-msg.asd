
(cl:in-package :asdf)

(defsystem "bag_recorder-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Rosbag" :depends-on ("_package_Rosbag"))
    (:file "_package_Rosbag" :depends-on ("_package"))
  ))