// Generated by gencpp from file bag_recorder/Rosbag.msg
// DO NOT EDIT!


#ifndef BAG_RECORDER_MESSAGE_ROSBAG_H
#define BAG_RECORDER_MESSAGE_ROSBAG_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace bag_recorder
{
template <class ContainerAllocator>
struct Rosbag_
{
  typedef Rosbag_<ContainerAllocator> Type;

  Rosbag_()
    : header()
    , config()
    , bag_name()  {
    }
  Rosbag_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , config(_alloc)
    , bag_name(_alloc)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _config_type;
  _config_type config;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _bag_name_type;
  _bag_name_type bag_name;





  typedef boost::shared_ptr< ::bag_recorder::Rosbag_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::bag_recorder::Rosbag_<ContainerAllocator> const> ConstPtr;

}; // struct Rosbag_

typedef ::bag_recorder::Rosbag_<std::allocator<void> > Rosbag;

typedef boost::shared_ptr< ::bag_recorder::Rosbag > RosbagPtr;
typedef boost::shared_ptr< ::bag_recorder::Rosbag const> RosbagConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::bag_recorder::Rosbag_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::bag_recorder::Rosbag_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::bag_recorder::Rosbag_<ContainerAllocator1> & lhs, const ::bag_recorder::Rosbag_<ContainerAllocator2> & rhs)
{
  return lhs.header == rhs.header &&
    lhs.config == rhs.config &&
    lhs.bag_name == rhs.bag_name;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::bag_recorder::Rosbag_<ContainerAllocator1> & lhs, const ::bag_recorder::Rosbag_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace bag_recorder

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::bag_recorder::Rosbag_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::bag_recorder::Rosbag_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::bag_recorder::Rosbag_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::bag_recorder::Rosbag_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::bag_recorder::Rosbag_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::bag_recorder::Rosbag_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::bag_recorder::Rosbag_<ContainerAllocator> >
{
  static const char* value()
  {
    return "6798439af7e30e1ec3307cb93f96c57b";
  }

  static const char* value(const ::bag_recorder::Rosbag_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x6798439af7e30e1eULL;
  static const uint64_t static_value2 = 0xc3307cb93f96c57bULL;
};

template<class ContainerAllocator>
struct DataType< ::bag_recorder::Rosbag_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bag_recorder/Rosbag";
  }

  static const char* value(const ::bag_recorder::Rosbag_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::bag_recorder::Rosbag_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n"
"\n"
"string config\n"
"string bag_name\n"
"\n"
"================================================================================\n"
"MSG: std_msgs/Header\n"
"# Standard metadata for higher-level stamped data types.\n"
"# This is generally used to communicate timestamped data \n"
"# in a particular coordinate frame.\n"
"# \n"
"# sequence ID: consecutively increasing ID \n"
"uint32 seq\n"
"#Two-integer timestamp that is expressed as:\n"
"# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n"
"# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n"
"# time-handling sugar is provided by the client library\n"
"time stamp\n"
"#Frame this data is associated with\n"
"string frame_id\n"
;
  }

  static const char* value(const ::bag_recorder::Rosbag_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::bag_recorder::Rosbag_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.config);
      stream.next(m.bag_name);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Rosbag_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::bag_recorder::Rosbag_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::bag_recorder::Rosbag_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "config: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.config);
    s << indent << "bag_name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.bag_name);
  }
};

} // namespace message_operations
} // namespace ros

#endif // BAG_RECORDER_MESSAGE_ROSBAG_H
