; Auto-generated. Do not edit!


(cl:in-package kombos_server-msg)


;//! \htmlinclude matrixMsg.msg.html

(cl:defclass <matrixMsg> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (cols
    :reader cols
    :initarg :cols
    :type cl:integer
    :initform 0)
   (rows
    :reader rows
    :initarg :rows
    :type cl:integer
    :initform 0)
   (M
    :reader M
    :initarg :M
    :type (cl:vector cl:float)
   :initform (cl:make-array 0 :element-type 'cl:float :initial-element 0.0)))
)

(cl:defclass matrixMsg (<matrixMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <matrixMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'matrixMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kombos_server-msg:<matrixMsg> is deprecated: use kombos_server-msg:matrixMsg instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <matrixMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:name-val is deprecated.  Use kombos_server-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'cols-val :lambda-list '(m))
(cl:defmethod cols-val ((m <matrixMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:cols-val is deprecated.  Use kombos_server-msg:cols instead.")
  (cols m))

(cl:ensure-generic-function 'rows-val :lambda-list '(m))
(cl:defmethod rows-val ((m <matrixMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:rows-val is deprecated.  Use kombos_server-msg:rows instead.")
  (rows m))

(cl:ensure-generic-function 'M-val :lambda-list '(m))
(cl:defmethod M-val ((m <matrixMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:M-val is deprecated.  Use kombos_server-msg:M instead.")
  (M m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <matrixMsg>) ostream)
  "Serializes a message object of type '<matrixMsg>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:let* ((signed (cl:slot-value msg 'cols)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'rows)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'M))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let ((bits (roslisp-utils:encode-double-float-bits ele)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream)))
   (cl:slot-value msg 'M))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <matrixMsg>) istream)
  "Deserializes a message object of type '<matrixMsg>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cols) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rows) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'M) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'M)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:aref vals i) (roslisp-utils:decode-double-float-bits bits))))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<matrixMsg>)))
  "Returns string type for a message object of type '<matrixMsg>"
  "kombos_server/matrixMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'matrixMsg)))
  "Returns string type for a message object of type 'matrixMsg"
  "kombos_server/matrixMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<matrixMsg>)))
  "Returns md5sum for a message object of type '<matrixMsg>"
  "8c1e478c7b06e7c44dca810a05c98a22")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'matrixMsg)))
  "Returns md5sum for a message object of type 'matrixMsg"
  "8c1e478c7b06e7c44dca810a05c98a22")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<matrixMsg>)))
  "Returns full string definition for message of type '<matrixMsg>"
  (cl:format cl:nil "string name~%int32 cols~%int32 rows~%float64[] M~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'matrixMsg)))
  "Returns full string definition for message of type 'matrixMsg"
  (cl:format cl:nil "string name~%int32 cols~%int32 rows~%float64[] M~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <matrixMsg>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     4
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'M) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 8)))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <matrixMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'matrixMsg
    (cl:cons ':name (name msg))
    (cl:cons ':cols (cols msg))
    (cl:cons ':rows (rows msg))
    (cl:cons ':M (M msg))
))
