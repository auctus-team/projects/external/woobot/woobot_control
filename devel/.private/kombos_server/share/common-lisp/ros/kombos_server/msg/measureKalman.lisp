; Auto-generated. Do not edit!


(cl:in-package kombos_server-msg)


;//! \htmlinclude measureKalman.msg.html

(cl:defclass <measureKalman> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type kombos_server-msg:measureKinect
    :initform (cl:make-instance 'kombos_server-msg:measureKinect))
   (matrices
    :reader matrices
    :initarg :matrices
    :type (cl:vector kombos_server-msg:matrixMsg)
   :initform (cl:make-array 0 :element-type 'kombos_server-msg:matrixMsg :initial-element (cl:make-instance 'kombos_server-msg:matrixMsg)))
   (state
    :reader state
    :initarg :state
    :type kombos_server-msg:kalmanState
    :initform (cl:make-instance 'kombos_server-msg:kalmanState)))
)

(cl:defclass measureKalman (<measureKalman>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <measureKalman>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'measureKalman)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kombos_server-msg:<measureKalman> is deprecated: use kombos_server-msg:measureKalman instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <measureKalman>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:data-val is deprecated.  Use kombos_server-msg:data instead.")
  (data m))

(cl:ensure-generic-function 'matrices-val :lambda-list '(m))
(cl:defmethod matrices-val ((m <measureKalman>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:matrices-val is deprecated.  Use kombos_server-msg:matrices instead.")
  (matrices m))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <measureKalman>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:state-val is deprecated.  Use kombos_server-msg:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <measureKalman>) ostream)
  "Serializes a message object of type '<measureKalman>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'data) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'matrices))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'matrices))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'state) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <measureKalman>) istream)
  "Deserializes a message object of type '<measureKalman>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'data) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'matrices) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'matrices)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'kombos_server-msg:matrixMsg))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'state) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<measureKalman>)))
  "Returns string type for a message object of type '<measureKalman>"
  "kombos_server/measureKalman")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'measureKalman)))
  "Returns string type for a message object of type 'measureKalman"
  "kombos_server/measureKalman")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<measureKalman>)))
  "Returns md5sum for a message object of type '<measureKalman>"
  "61fc728b332661d91b29e3f5c4fe0dd6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'measureKalman)))
  "Returns md5sum for a message object of type 'measureKalman"
  "61fc728b332661d91b29e3f5c4fe0dd6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<measureKalman>)))
  "Returns full string definition for message of type '<measureKalman>"
  (cl:format cl:nil "measureKinect data~%matrixMsg[] matrices~%kalmanState state~%~%================================================================================~%MSG: kombos_server/measureKinect~%string typeCapteur~%int32 idCapteur~%positionMsg[] measures~%~%================================================================================~%MSG: kombos_server/positionMsg~%string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: kombos_server/matrixMsg~%string name~%int32 cols~%int32 rows~%float64[] M~%================================================================================~%MSG: kombos_server/kalmanState~%string[] name~%float32[] value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'measureKalman)))
  "Returns full string definition for message of type 'measureKalman"
  (cl:format cl:nil "measureKinect data~%matrixMsg[] matrices~%kalmanState state~%~%================================================================================~%MSG: kombos_server/measureKinect~%string typeCapteur~%int32 idCapteur~%positionMsg[] measures~%~%================================================================================~%MSG: kombos_server/positionMsg~%string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: kombos_server/matrixMsg~%string name~%int32 cols~%int32 rows~%float64[] M~%================================================================================~%MSG: kombos_server/kalmanState~%string[] name~%float32[] value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <measureKalman>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'data))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'matrices) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'state))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <measureKalman>))
  "Converts a ROS message object to a list"
  (cl:list 'measureKalman
    (cl:cons ':data (data msg))
    (cl:cons ':matrices (matrices msg))
    (cl:cons ':state (state msg))
))
