
(cl:in-package :asdf)

(defsystem "kombos_server-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
)
  :components ((:file "_package")
    (:file "kalmanState" :depends-on ("_package_kalmanState"))
    (:file "_package_kalmanState" :depends-on ("_package"))
    (:file "matrixMsg" :depends-on ("_package_matrixMsg"))
    (:file "_package_matrixMsg" :depends-on ("_package"))
    (:file "measureKalman" :depends-on ("_package_measureKalman"))
    (:file "_package_measureKalman" :depends-on ("_package"))
    (:file "measureKinect" :depends-on ("_package_measureKinect"))
    (:file "_package_measureKinect" :depends-on ("_package"))
    (:file "positionMsg" :depends-on ("_package_positionMsg"))
    (:file "_package_positionMsg" :depends-on ("_package"))
  ))