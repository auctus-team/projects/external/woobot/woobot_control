; Auto-generated. Do not edit!


(cl:in-package kombos_server-msg)


;//! \htmlinclude measureKinect.msg.html

(cl:defclass <measureKinect> (roslisp-msg-protocol:ros-message)
  ((typeCapteur
    :reader typeCapteur
    :initarg :typeCapteur
    :type cl:string
    :initform "")
   (idCapteur
    :reader idCapteur
    :initarg :idCapteur
    :type cl:integer
    :initform 0)
   (measures
    :reader measures
    :initarg :measures
    :type (cl:vector kombos_server-msg:positionMsg)
   :initform (cl:make-array 0 :element-type 'kombos_server-msg:positionMsg :initial-element (cl:make-instance 'kombos_server-msg:positionMsg))))
)

(cl:defclass measureKinect (<measureKinect>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <measureKinect>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'measureKinect)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kombos_server-msg:<measureKinect> is deprecated: use kombos_server-msg:measureKinect instead.")))

(cl:ensure-generic-function 'typeCapteur-val :lambda-list '(m))
(cl:defmethod typeCapteur-val ((m <measureKinect>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:typeCapteur-val is deprecated.  Use kombos_server-msg:typeCapteur instead.")
  (typeCapteur m))

(cl:ensure-generic-function 'idCapteur-val :lambda-list '(m))
(cl:defmethod idCapteur-val ((m <measureKinect>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:idCapteur-val is deprecated.  Use kombos_server-msg:idCapteur instead.")
  (idCapteur m))

(cl:ensure-generic-function 'measures-val :lambda-list '(m))
(cl:defmethod measures-val ((m <measureKinect>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:measures-val is deprecated.  Use kombos_server-msg:measures instead.")
  (measures m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <measureKinect>) ostream)
  "Serializes a message object of type '<measureKinect>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'typeCapteur))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'typeCapteur))
  (cl:let* ((signed (cl:slot-value msg 'idCapteur)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'measures))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'measures))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <measureKinect>) istream)
  "Deserializes a message object of type '<measureKinect>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'typeCapteur) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'typeCapteur) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'idCapteur) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'measures) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'measures)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'kombos_server-msg:positionMsg))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<measureKinect>)))
  "Returns string type for a message object of type '<measureKinect>"
  "kombos_server/measureKinect")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'measureKinect)))
  "Returns string type for a message object of type 'measureKinect"
  "kombos_server/measureKinect")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<measureKinect>)))
  "Returns md5sum for a message object of type '<measureKinect>"
  "960a1c5ad49ec3881fed79eb9f4a75e1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'measureKinect)))
  "Returns md5sum for a message object of type 'measureKinect"
  "960a1c5ad49ec3881fed79eb9f4a75e1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<measureKinect>)))
  "Returns full string definition for message of type '<measureKinect>"
  (cl:format cl:nil "string typeCapteur~%int32 idCapteur~%positionMsg[] measures~%~%================================================================================~%MSG: kombos_server/positionMsg~%string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'measureKinect)))
  "Returns full string definition for message of type 'measureKinect"
  (cl:format cl:nil "string typeCapteur~%int32 idCapteur~%positionMsg[] measures~%~%================================================================================~%MSG: kombos_server/positionMsg~%string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <measureKinect>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'typeCapteur))
     4
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'measures) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <measureKinect>))
  "Converts a ROS message object to a list"
  (cl:list 'measureKinect
    (cl:cons ':typeCapteur (typeCapteur msg))
    (cl:cons ':idCapteur (idCapteur msg))
    (cl:cons ':measures (measures msg))
))
