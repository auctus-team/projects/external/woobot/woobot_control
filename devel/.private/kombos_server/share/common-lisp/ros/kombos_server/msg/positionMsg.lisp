; Auto-generated. Do not edit!


(cl:in-package kombos_server-msg)


;//! \htmlinclude positionMsg.msg.html

(cl:defclass <positionMsg> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (position
    :reader position
    :initarg :position
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (fiability
    :reader fiability
    :initarg :fiability
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass positionMsg (<positionMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <positionMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'positionMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kombos_server-msg:<positionMsg> is deprecated: use kombos_server-msg:positionMsg instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <positionMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:name-val is deprecated.  Use kombos_server-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <positionMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:position-val is deprecated.  Use kombos_server-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'fiability-val :lambda-list '(m))
(cl:defmethod fiability-val ((m <positionMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kombos_server-msg:fiability-val is deprecated.  Use kombos_server-msg:fiability instead.")
  (fiability m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <positionMsg>) ostream)
  "Serializes a message object of type '<positionMsg>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'fiability) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <positionMsg>) istream)
  "Deserializes a message object of type '<positionMsg>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
    (cl:setf (cl:slot-value msg 'fiability) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<positionMsg>)))
  "Returns string type for a message object of type '<positionMsg>"
  "kombos_server/positionMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'positionMsg)))
  "Returns string type for a message object of type 'positionMsg"
  "kombos_server/positionMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<positionMsg>)))
  "Returns md5sum for a message object of type '<positionMsg>"
  "1ae4ef2f61a475f4bebc8aa3b748ebd6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'positionMsg)))
  "Returns md5sum for a message object of type 'positionMsg"
  "1ae4ef2f61a475f4bebc8aa3b748ebd6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<positionMsg>)))
  "Returns full string definition for message of type '<positionMsg>"
  (cl:format cl:nil "string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'positionMsg)))
  "Returns full string definition for message of type 'positionMsg"
  (cl:format cl:nil "string name~%geometry_msgs/Point position~%bool fiability~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <positionMsg>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <positionMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'positionMsg
    (cl:cons ':name (name msg))
    (cl:cons ':position (position msg))
    (cl:cons ':fiability (fiability msg))
))
