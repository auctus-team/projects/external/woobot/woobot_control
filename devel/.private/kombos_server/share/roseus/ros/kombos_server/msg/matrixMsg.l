;; Auto-generated. Do not edit!


(when (boundp 'kombos_server::matrixMsg)
  (if (not (find-package "KOMBOS_SERVER"))
    (make-package "KOMBOS_SERVER"))
  (shadow 'matrixMsg (find-package "KOMBOS_SERVER")))
(unless (find-package "KOMBOS_SERVER::MATRIXMSG")
  (make-package "KOMBOS_SERVER::MATRIXMSG"))

(in-package "ROS")
;;//! \htmlinclude matrixMsg.msg.html


(defclass kombos_server::matrixMsg
  :super ros::object
  :slots (_name _cols _rows _M ))

(defmethod kombos_server::matrixMsg
  (:init
   (&key
    ((:name __name) "")
    ((:cols __cols) 0)
    ((:rows __rows) 0)
    ((:M __M) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _cols (round __cols))
   (setq _rows (round __rows))
   (setq _M __M)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:cols
   (&optional __cols)
   (if __cols (setq _cols __cols)) _cols)
  (:rows
   (&optional __rows)
   (if __rows (setq _rows __rows)) _rows)
  (:M
   (&optional __M)
   (if __M (setq _M __M)) _M)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; int32 _cols
    4
    ;; int32 _rows
    4
    ;; float64[] _M
    (* 8    (length _M)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; int32 _cols
       (write-long _cols s)
     ;; int32 _rows
       (write-long _rows s)
     ;; float64[] _M
     (write-long (length _M) s)
     (dotimes (i (length _M))
       (sys::poke (elt _M i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32 _cols
     (setq _cols (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _rows
     (setq _rows (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float64[] _M
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _M (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _M i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;;
   self)
  )

(setf (get kombos_server::matrixMsg :md5sum-) "8c1e478c7b06e7c44dca810a05c98a22")
(setf (get kombos_server::matrixMsg :datatype-) "kombos_server/matrixMsg")
(setf (get kombos_server::matrixMsg :definition-)
      "string name
int32 cols
int32 rows
float64[] M
")



(provide :kombos_server/matrixMsg "8c1e478c7b06e7c44dca810a05c98a22")


