;; Auto-generated. Do not edit!


(when (boundp 'kombos_server::measureKalman)
  (if (not (find-package "KOMBOS_SERVER"))
    (make-package "KOMBOS_SERVER"))
  (shadow 'measureKalman (find-package "KOMBOS_SERVER")))
(unless (find-package "KOMBOS_SERVER::MEASUREKALMAN")
  (make-package "KOMBOS_SERVER::MEASUREKALMAN"))

(in-package "ROS")
;;//! \htmlinclude measureKalman.msg.html


(defclass kombos_server::measureKalman
  :super ros::object
  :slots (_data _matrices _state ))

(defmethod kombos_server::measureKalman
  (:init
   (&key
    ((:data __data) (instance kombos_server::measureKinect :init))
    ((:matrices __matrices) (let (r) (dotimes (i 0) (push (instance kombos_server::matrixMsg :init) r)) r))
    ((:state __state) (instance kombos_server::kalmanState :init))
    )
   (send-super :init)
   (setq _data __data)
   (setq _matrices __matrices)
   (setq _state __state)
   self)
  (:data
   (&rest __data)
   (if (keywordp (car __data))
       (send* _data __data)
     (progn
       (if __data (setq _data (car __data)))
       _data)))
  (:matrices
   (&rest __matrices)
   (if (keywordp (car __matrices))
       (send* _matrices __matrices)
     (progn
       (if __matrices (setq _matrices (car __matrices)))
       _matrices)))
  (:state
   (&rest __state)
   (if (keywordp (car __state))
       (send* _state __state)
     (progn
       (if __state (setq _state (car __state)))
       _state)))
  (:serialization-length
   ()
   (+
    ;; kombos_server/measureKinect _data
    (send _data :serialization-length)
    ;; kombos_server/matrixMsg[] _matrices
    (apply #'+ (send-all _matrices :serialization-length)) 4
    ;; kombos_server/kalmanState _state
    (send _state :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; kombos_server/measureKinect _data
       (send _data :serialize s)
     ;; kombos_server/matrixMsg[] _matrices
     (write-long (length _matrices) s)
     (dolist (elem _matrices)
       (send elem :serialize s)
       )
     ;; kombos_server/kalmanState _state
       (send _state :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; kombos_server/measureKinect _data
     (send _data :deserialize buf ptr-) (incf ptr- (send _data :serialization-length))
   ;; kombos_server/matrixMsg[] _matrices
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _matrices (let (r) (dotimes (i n) (push (instance kombos_server::matrixMsg :init) r)) r))
     (dolist (elem- _matrices)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; kombos_server/kalmanState _state
     (send _state :deserialize buf ptr-) (incf ptr- (send _state :serialization-length))
   ;;
   self)
  )

(setf (get kombos_server::measureKalman :md5sum-) "61fc728b332661d91b29e3f5c4fe0dd6")
(setf (get kombos_server::measureKalman :datatype-) "kombos_server/measureKalman")
(setf (get kombos_server::measureKalman :definition-)
      "measureKinect data
matrixMsg[] matrices
kalmanState state

================================================================================
MSG: kombos_server/measureKinect
string typeCapteur
int32 idCapteur
positionMsg[] measures

================================================================================
MSG: kombos_server/positionMsg
string name
geometry_msgs/Point position
bool fiability

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: kombos_server/matrixMsg
string name
int32 cols
int32 rows
float64[] M
================================================================================
MSG: kombos_server/kalmanState
string[] name
float32[] value

")



(provide :kombos_server/measureKalman "61fc728b332661d91b29e3f5c4fe0dd6")


