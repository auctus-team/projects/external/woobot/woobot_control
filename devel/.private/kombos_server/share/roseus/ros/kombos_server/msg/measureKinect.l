;; Auto-generated. Do not edit!


(when (boundp 'kombos_server::measureKinect)
  (if (not (find-package "KOMBOS_SERVER"))
    (make-package "KOMBOS_SERVER"))
  (shadow 'measureKinect (find-package "KOMBOS_SERVER")))
(unless (find-package "KOMBOS_SERVER::MEASUREKINECT")
  (make-package "KOMBOS_SERVER::MEASUREKINECT"))

(in-package "ROS")
;;//! \htmlinclude measureKinect.msg.html


(defclass kombos_server::measureKinect
  :super ros::object
  :slots (_typeCapteur _idCapteur _measures ))

(defmethod kombos_server::measureKinect
  (:init
   (&key
    ((:typeCapteur __typeCapteur) "")
    ((:idCapteur __idCapteur) 0)
    ((:measures __measures) (let (r) (dotimes (i 0) (push (instance kombos_server::positionMsg :init) r)) r))
    )
   (send-super :init)
   (setq _typeCapteur (string __typeCapteur))
   (setq _idCapteur (round __idCapteur))
   (setq _measures __measures)
   self)
  (:typeCapteur
   (&optional __typeCapteur)
   (if __typeCapteur (setq _typeCapteur __typeCapteur)) _typeCapteur)
  (:idCapteur
   (&optional __idCapteur)
   (if __idCapteur (setq _idCapteur __idCapteur)) _idCapteur)
  (:measures
   (&rest __measures)
   (if (keywordp (car __measures))
       (send* _measures __measures)
     (progn
       (if __measures (setq _measures (car __measures)))
       _measures)))
  (:serialization-length
   ()
   (+
    ;; string _typeCapteur
    4 (length _typeCapteur)
    ;; int32 _idCapteur
    4
    ;; kombos_server/positionMsg[] _measures
    (apply #'+ (send-all _measures :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _typeCapteur
       (write-long (length _typeCapteur) s) (princ _typeCapteur s)
     ;; int32 _idCapteur
       (write-long _idCapteur s)
     ;; kombos_server/positionMsg[] _measures
     (write-long (length _measures) s)
     (dolist (elem _measures)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _typeCapteur
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _typeCapteur (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32 _idCapteur
     (setq _idCapteur (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; kombos_server/positionMsg[] _measures
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _measures (let (r) (dotimes (i n) (push (instance kombos_server::positionMsg :init) r)) r))
     (dolist (elem- _measures)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get kombos_server::measureKinect :md5sum-) "960a1c5ad49ec3881fed79eb9f4a75e1")
(setf (get kombos_server::measureKinect :datatype-) "kombos_server/measureKinect")
(setf (get kombos_server::measureKinect :definition-)
      "string typeCapteur
int32 idCapteur
positionMsg[] measures

================================================================================
MSG: kombos_server/positionMsg
string name
geometry_msgs/Point position
bool fiability

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

")



(provide :kombos_server/measureKinect "960a1c5ad49ec3881fed79eb9f4a75e1")


