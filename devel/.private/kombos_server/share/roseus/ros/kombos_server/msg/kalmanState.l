;; Auto-generated. Do not edit!


(when (boundp 'kombos_server::kalmanState)
  (if (not (find-package "KOMBOS_SERVER"))
    (make-package "KOMBOS_SERVER"))
  (shadow 'kalmanState (find-package "KOMBOS_SERVER")))
(unless (find-package "KOMBOS_SERVER::KALMANSTATE")
  (make-package "KOMBOS_SERVER::KALMANSTATE"))

(in-package "ROS")
;;//! \htmlinclude kalmanState.msg.html


(defclass kombos_server::kalmanState
  :super ros::object
  :slots (_name _value ))

(defmethod kombos_server::kalmanState
  (:init
   (&key
    ((:name __name) (let (r) (dotimes (i 0) (push "" r)) r))
    ((:value __value) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _name __name)
   (setq _value __value)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; string[] _name
    (apply #'+ (mapcar #'(lambda (x) (+ 4 (length x))) _name)) 4
    ;; float32[] _value
    (* 4    (length _value)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string[] _name
     (write-long (length _name) s)
     (dolist (elem _name)
       (write-long (length elem) s) (princ elem s)
       )
     ;; float32[] _value
     (write-long (length _value) s)
     (dotimes (i (length _value))
       (sys::poke (elt _value i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string[] _name
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _name (make-list n))
     (dotimes (i n)
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setf (elt _name i) (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
     ))
   ;; float32[] _value
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _value (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _value i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;;
   self)
  )

(setf (get kombos_server::kalmanState :md5sum-) "bba10c1f9b54c4a91b40e94b102c2543")
(setf (get kombos_server::kalmanState :datatype-) "kombos_server/kalmanState")
(setf (get kombos_server::kalmanState :definition-)
      "string[] name
float32[] value

")



(provide :kombos_server/kalmanState "bba10c1f9b54c4a91b40e94b102c2543")


