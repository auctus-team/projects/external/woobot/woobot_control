;; Auto-generated. Do not edit!


(when (boundp 'kombos_server::positionMsg)
  (if (not (find-package "KOMBOS_SERVER"))
    (make-package "KOMBOS_SERVER"))
  (shadow 'positionMsg (find-package "KOMBOS_SERVER")))
(unless (find-package "KOMBOS_SERVER::POSITIONMSG")
  (make-package "KOMBOS_SERVER::POSITIONMSG"))

(in-package "ROS")
;;//! \htmlinclude positionMsg.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))


(defclass kombos_server::positionMsg
  :super ros::object
  :slots (_name _position _fiability ))

(defmethod kombos_server::positionMsg
  (:init
   (&key
    ((:name __name) "")
    ((:position __position) (instance geometry_msgs::Point :init))
    ((:fiability __fiability) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _position __position)
   (setq _fiability __fiability)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:fiability
   (&optional __fiability)
   (if __fiability (setq _fiability __fiability)) _fiability)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; geometry_msgs/Point _position
    (send _position :serialization-length)
    ;; bool _fiability
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; geometry_msgs/Point _position
       (send _position :serialize s)
     ;; bool _fiability
       (if _fiability (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; geometry_msgs/Point _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; bool _fiability
     (setq _fiability (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get kombos_server::positionMsg :md5sum-) "1ae4ef2f61a475f4bebc8aa3b748ebd6")
(setf (get kombos_server::positionMsg :datatype-) "kombos_server/positionMsg")
(setf (get kombos_server::positionMsg :definition-)
      "string name
geometry_msgs/Point position
bool fiability

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

")



(provide :kombos_server/positionMsg "1ae4ef2f61a475f4bebc8aa3b748ebd6")


