// Auto-generated. Do not edit!

// (in-package kombos_server.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class matrixMsg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.name = null;
      this.cols = null;
      this.rows = null;
      this.M = null;
    }
    else {
      if (initObj.hasOwnProperty('name')) {
        this.name = initObj.name
      }
      else {
        this.name = '';
      }
      if (initObj.hasOwnProperty('cols')) {
        this.cols = initObj.cols
      }
      else {
        this.cols = 0;
      }
      if (initObj.hasOwnProperty('rows')) {
        this.rows = initObj.rows
      }
      else {
        this.rows = 0;
      }
      if (initObj.hasOwnProperty('M')) {
        this.M = initObj.M
      }
      else {
        this.M = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type matrixMsg
    // Serialize message field [name]
    bufferOffset = _serializer.string(obj.name, buffer, bufferOffset);
    // Serialize message field [cols]
    bufferOffset = _serializer.int32(obj.cols, buffer, bufferOffset);
    // Serialize message field [rows]
    bufferOffset = _serializer.int32(obj.rows, buffer, bufferOffset);
    // Serialize message field [M]
    bufferOffset = _arraySerializer.float64(obj.M, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type matrixMsg
    let len;
    let data = new matrixMsg(null);
    // Deserialize message field [name]
    data.name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [cols]
    data.cols = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [rows]
    data.rows = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [M]
    data.M = _arrayDeserializer.float64(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.name.length;
    length += 8 * object.M.length;
    return length + 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'kombos_server/matrixMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8c1e478c7b06e7c44dca810a05c98a22';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string name
    int32 cols
    int32 rows
    float64[] M
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new matrixMsg(null);
    if (msg.name !== undefined) {
      resolved.name = msg.name;
    }
    else {
      resolved.name = ''
    }

    if (msg.cols !== undefined) {
      resolved.cols = msg.cols;
    }
    else {
      resolved.cols = 0
    }

    if (msg.rows !== undefined) {
      resolved.rows = msg.rows;
    }
    else {
      resolved.rows = 0
    }

    if (msg.M !== undefined) {
      resolved.M = msg.M;
    }
    else {
      resolved.M = []
    }

    return resolved;
    }
};

module.exports = matrixMsg;
