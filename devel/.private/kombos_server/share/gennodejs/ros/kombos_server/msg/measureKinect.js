// Auto-generated. Do not edit!

// (in-package kombos_server.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let positionMsg = require('./positionMsg.js');

//-----------------------------------------------------------

class measureKinect {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.typeCapteur = null;
      this.idCapteur = null;
      this.measures = null;
    }
    else {
      if (initObj.hasOwnProperty('typeCapteur')) {
        this.typeCapteur = initObj.typeCapteur
      }
      else {
        this.typeCapteur = '';
      }
      if (initObj.hasOwnProperty('idCapteur')) {
        this.idCapteur = initObj.idCapteur
      }
      else {
        this.idCapteur = 0;
      }
      if (initObj.hasOwnProperty('measures')) {
        this.measures = initObj.measures
      }
      else {
        this.measures = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type measureKinect
    // Serialize message field [typeCapteur]
    bufferOffset = _serializer.string(obj.typeCapteur, buffer, bufferOffset);
    // Serialize message field [idCapteur]
    bufferOffset = _serializer.int32(obj.idCapteur, buffer, bufferOffset);
    // Serialize message field [measures]
    // Serialize the length for message field [measures]
    bufferOffset = _serializer.uint32(obj.measures.length, buffer, bufferOffset);
    obj.measures.forEach((val) => {
      bufferOffset = positionMsg.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type measureKinect
    let len;
    let data = new measureKinect(null);
    // Deserialize message field [typeCapteur]
    data.typeCapteur = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [idCapteur]
    data.idCapteur = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [measures]
    // Deserialize array length for message field [measures]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.measures = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.measures[i] = positionMsg.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.typeCapteur.length;
    object.measures.forEach((val) => {
      length += positionMsg.getMessageSize(val);
    });
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'kombos_server/measureKinect';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '960a1c5ad49ec3881fed79eb9f4a75e1';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string typeCapteur
    int32 idCapteur
    positionMsg[] measures
    
    ================================================================================
    MSG: kombos_server/positionMsg
    string name
    geometry_msgs/Point position
    bool fiability
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new measureKinect(null);
    if (msg.typeCapteur !== undefined) {
      resolved.typeCapteur = msg.typeCapteur;
    }
    else {
      resolved.typeCapteur = ''
    }

    if (msg.idCapteur !== undefined) {
      resolved.idCapteur = msg.idCapteur;
    }
    else {
      resolved.idCapteur = 0
    }

    if (msg.measures !== undefined) {
      resolved.measures = new Array(msg.measures.length);
      for (let i = 0; i < resolved.measures.length; ++i) {
        resolved.measures[i] = positionMsg.Resolve(msg.measures[i]);
      }
    }
    else {
      resolved.measures = []
    }

    return resolved;
    }
};

module.exports = measureKinect;
