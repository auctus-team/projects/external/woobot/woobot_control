// Auto-generated. Do not edit!

// (in-package kombos_server.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let measureKinect = require('./measureKinect.js');
let matrixMsg = require('./matrixMsg.js');
let kalmanState = require('./kalmanState.js');

//-----------------------------------------------------------

class measureKalman {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.data = null;
      this.matrices = null;
      this.state = null;
    }
    else {
      if (initObj.hasOwnProperty('data')) {
        this.data = initObj.data
      }
      else {
        this.data = new measureKinect();
      }
      if (initObj.hasOwnProperty('matrices')) {
        this.matrices = initObj.matrices
      }
      else {
        this.matrices = [];
      }
      if (initObj.hasOwnProperty('state')) {
        this.state = initObj.state
      }
      else {
        this.state = new kalmanState();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type measureKalman
    // Serialize message field [data]
    bufferOffset = measureKinect.serialize(obj.data, buffer, bufferOffset);
    // Serialize message field [matrices]
    // Serialize the length for message field [matrices]
    bufferOffset = _serializer.uint32(obj.matrices.length, buffer, bufferOffset);
    obj.matrices.forEach((val) => {
      bufferOffset = matrixMsg.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [state]
    bufferOffset = kalmanState.serialize(obj.state, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type measureKalman
    let len;
    let data = new measureKalman(null);
    // Deserialize message field [data]
    data.data = measureKinect.deserialize(buffer, bufferOffset);
    // Deserialize message field [matrices]
    // Deserialize array length for message field [matrices]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.matrices = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.matrices[i] = matrixMsg.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [state]
    data.state = kalmanState.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += measureKinect.getMessageSize(object.data);
    object.matrices.forEach((val) => {
      length += matrixMsg.getMessageSize(val);
    });
    length += kalmanState.getMessageSize(object.state);
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'kombos_server/measureKalman';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '61fc728b332661d91b29e3f5c4fe0dd6';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    measureKinect data
    matrixMsg[] matrices
    kalmanState state
    
    ================================================================================
    MSG: kombos_server/measureKinect
    string typeCapteur
    int32 idCapteur
    positionMsg[] measures
    
    ================================================================================
    MSG: kombos_server/positionMsg
    string name
    geometry_msgs/Point position
    bool fiability
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: kombos_server/matrixMsg
    string name
    int32 cols
    int32 rows
    float64[] M
    ================================================================================
    MSG: kombos_server/kalmanState
    string[] name
    float32[] value
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new measureKalman(null);
    if (msg.data !== undefined) {
      resolved.data = measureKinect.Resolve(msg.data)
    }
    else {
      resolved.data = new measureKinect()
    }

    if (msg.matrices !== undefined) {
      resolved.matrices = new Array(msg.matrices.length);
      for (let i = 0; i < resolved.matrices.length; ++i) {
        resolved.matrices[i] = matrixMsg.Resolve(msg.matrices[i]);
      }
    }
    else {
      resolved.matrices = []
    }

    if (msg.state !== undefined) {
      resolved.state = kalmanState.Resolve(msg.state)
    }
    else {
      resolved.state = new kalmanState()
    }

    return resolved;
    }
};

module.exports = measureKalman;
