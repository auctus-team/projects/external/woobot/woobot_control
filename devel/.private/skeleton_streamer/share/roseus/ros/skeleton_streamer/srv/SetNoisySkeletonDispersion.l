;; Auto-generated. Do not edit!


(when (boundp 'skeleton_streamer::SetNoisySkeletonDispersion)
  (if (not (find-package "SKELETON_STREAMER"))
    (make-package "SKELETON_STREAMER"))
  (shadow 'SetNoisySkeletonDispersion (find-package "SKELETON_STREAMER")))
(unless (find-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSION")
  (make-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSION"))
(unless (find-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSIONREQUEST")
  (make-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSIONREQUEST"))
(unless (find-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSIONRESPONSE")
  (make-package "SKELETON_STREAMER::SETNOISYSKELETONDISPERSIONRESPONSE"))

(in-package "ROS")





(defclass skeleton_streamer::SetNoisySkeletonDispersionRequest
  :super ros::object
  :slots (_data ))

(defmethod skeleton_streamer::SetNoisySkeletonDispersionRequest
  (:init
   (&key
    ((:data __data) 0.0)
    )
   (send-super :init)
   (setq _data (float __data))
   self)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; float32 _data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _data
       (sys::poke _data (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _data
     (setq _data (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass skeleton_streamer::SetNoisySkeletonDispersionResponse
  :super ros::object
  :slots (_data ))

(defmethod skeleton_streamer::SetNoisySkeletonDispersionResponse
  (:init
   (&key
    ((:data __data) 0.0)
    )
   (send-super :init)
   (setq _data (float __data))
   self)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:serialization-length
   ()
   (+
    ;; float32 _data
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _data
       (sys::poke _data (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _data
     (setq _data (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass skeleton_streamer::SetNoisySkeletonDispersion
  :super ros::object
  :slots ())

(setf (get skeleton_streamer::SetNoisySkeletonDispersion :md5sum-) "e455171ff03bf128b1cfbfb948576080")
(setf (get skeleton_streamer::SetNoisySkeletonDispersion :datatype-) "skeleton_streamer/SetNoisySkeletonDispersion")
(setf (get skeleton_streamer::SetNoisySkeletonDispersion :request) skeleton_streamer::SetNoisySkeletonDispersionRequest)
(setf (get skeleton_streamer::SetNoisySkeletonDispersion :response) skeleton_streamer::SetNoisySkeletonDispersionResponse)

(defmethod skeleton_streamer::SetNoisySkeletonDispersionRequest
  (:response () (instance skeleton_streamer::SetNoisySkeletonDispersionResponse :init)))

(setf (get skeleton_streamer::SetNoisySkeletonDispersionRequest :md5sum-) "e455171ff03bf128b1cfbfb948576080")
(setf (get skeleton_streamer::SetNoisySkeletonDispersionRequest :datatype-) "skeleton_streamer/SetNoisySkeletonDispersionRequest")
(setf (get skeleton_streamer::SetNoisySkeletonDispersionRequest :definition-)
      "float32 data
---
float32 data

")

(setf (get skeleton_streamer::SetNoisySkeletonDispersionResponse :md5sum-) "e455171ff03bf128b1cfbfb948576080")
(setf (get skeleton_streamer::SetNoisySkeletonDispersionResponse :datatype-) "skeleton_streamer/SetNoisySkeletonDispersionResponse")
(setf (get skeleton_streamer::SetNoisySkeletonDispersionResponse :definition-)
      "float32 data
---
float32 data

")



(provide :skeleton_streamer/SetNoisySkeletonDispersion "e455171ff03bf128b1cfbfb948576080")


