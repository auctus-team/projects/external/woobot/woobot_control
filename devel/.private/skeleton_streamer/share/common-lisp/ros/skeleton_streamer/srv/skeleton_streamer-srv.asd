
(cl:in-package :asdf)

(defsystem "skeleton_streamer-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "SetNoisySkeletonDispersion" :depends-on ("_package_SetNoisySkeletonDispersion"))
    (:file "_package_SetNoisySkeletonDispersion" :depends-on ("_package"))
  ))