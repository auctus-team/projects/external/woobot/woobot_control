; Auto-generated. Do not edit!


(cl:in-package skeleton_streamer-srv)


;//! \htmlinclude SetNoisySkeletonDispersion-request.msg.html

(cl:defclass <SetNoisySkeletonDispersion-request> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type cl:float
    :initform 0.0))
)

(cl:defclass SetNoisySkeletonDispersion-request (<SetNoisySkeletonDispersion-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetNoisySkeletonDispersion-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetNoisySkeletonDispersion-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name skeleton_streamer-srv:<SetNoisySkeletonDispersion-request> is deprecated: use skeleton_streamer-srv:SetNoisySkeletonDispersion-request instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <SetNoisySkeletonDispersion-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader skeleton_streamer-srv:data-val is deprecated.  Use skeleton_streamer-srv:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetNoisySkeletonDispersion-request>) ostream)
  "Serializes a message object of type '<SetNoisySkeletonDispersion-request>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetNoisySkeletonDispersion-request>) istream)
  "Deserializes a message object of type '<SetNoisySkeletonDispersion-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'data) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetNoisySkeletonDispersion-request>)))
  "Returns string type for a service object of type '<SetNoisySkeletonDispersion-request>"
  "skeleton_streamer/SetNoisySkeletonDispersionRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNoisySkeletonDispersion-request)))
  "Returns string type for a service object of type 'SetNoisySkeletonDispersion-request"
  "skeleton_streamer/SetNoisySkeletonDispersionRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetNoisySkeletonDispersion-request>)))
  "Returns md5sum for a message object of type '<SetNoisySkeletonDispersion-request>"
  "e455171ff03bf128b1cfbfb948576080")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetNoisySkeletonDispersion-request)))
  "Returns md5sum for a message object of type 'SetNoisySkeletonDispersion-request"
  "e455171ff03bf128b1cfbfb948576080")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetNoisySkeletonDispersion-request>)))
  "Returns full string definition for message of type '<SetNoisySkeletonDispersion-request>"
  (cl:format cl:nil "float32 data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetNoisySkeletonDispersion-request)))
  "Returns full string definition for message of type 'SetNoisySkeletonDispersion-request"
  (cl:format cl:nil "float32 data~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetNoisySkeletonDispersion-request>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetNoisySkeletonDispersion-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetNoisySkeletonDispersion-request
    (cl:cons ':data (data msg))
))
;//! \htmlinclude SetNoisySkeletonDispersion-response.msg.html

(cl:defclass <SetNoisySkeletonDispersion-response> (roslisp-msg-protocol:ros-message)
  ((data
    :reader data
    :initarg :data
    :type cl:float
    :initform 0.0))
)

(cl:defclass SetNoisySkeletonDispersion-response (<SetNoisySkeletonDispersion-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetNoisySkeletonDispersion-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetNoisySkeletonDispersion-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name skeleton_streamer-srv:<SetNoisySkeletonDispersion-response> is deprecated: use skeleton_streamer-srv:SetNoisySkeletonDispersion-response instead.")))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <SetNoisySkeletonDispersion-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader skeleton_streamer-srv:data-val is deprecated.  Use skeleton_streamer-srv:data instead.")
  (data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetNoisySkeletonDispersion-response>) ostream)
  "Serializes a message object of type '<SetNoisySkeletonDispersion-response>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'data))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetNoisySkeletonDispersion-response>) istream)
  "Deserializes a message object of type '<SetNoisySkeletonDispersion-response>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'data) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetNoisySkeletonDispersion-response>)))
  "Returns string type for a service object of type '<SetNoisySkeletonDispersion-response>"
  "skeleton_streamer/SetNoisySkeletonDispersionResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNoisySkeletonDispersion-response)))
  "Returns string type for a service object of type 'SetNoisySkeletonDispersion-response"
  "skeleton_streamer/SetNoisySkeletonDispersionResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetNoisySkeletonDispersion-response>)))
  "Returns md5sum for a message object of type '<SetNoisySkeletonDispersion-response>"
  "e455171ff03bf128b1cfbfb948576080")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetNoisySkeletonDispersion-response)))
  "Returns md5sum for a message object of type 'SetNoisySkeletonDispersion-response"
  "e455171ff03bf128b1cfbfb948576080")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetNoisySkeletonDispersion-response>)))
  "Returns full string definition for message of type '<SetNoisySkeletonDispersion-response>"
  (cl:format cl:nil "float32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetNoisySkeletonDispersion-response)))
  "Returns full string definition for message of type 'SetNoisySkeletonDispersion-response"
  (cl:format cl:nil "float32 data~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetNoisySkeletonDispersion-response>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetNoisySkeletonDispersion-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetNoisySkeletonDispersion-response
    (cl:cons ':data (data msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetNoisySkeletonDispersion)))
  'SetNoisySkeletonDispersion-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetNoisySkeletonDispersion)))
  'SetNoisySkeletonDispersion-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetNoisySkeletonDispersion)))
  "Returns string type for a service object of type '<SetNoisySkeletonDispersion>"
  "skeleton_streamer/SetNoisySkeletonDispersion")